from django.contrib import admin

from heroes.models import Ability

admin.site.register(Ability)

# Register your models here.
