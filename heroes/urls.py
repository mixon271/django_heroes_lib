from django.conf.urls import patterns, url
from heroes import views

urlpatterns = patterns('',
        url(r'^$', views.ability_index, name='index'),
        url(r'^(?P<id>\d+)/$', views.ability_detail, name='detail'),
        url(r'^new/$', views.ability_new, name='new'),
        url(r'^edit/(?P<id>\d+)/$', views.ability_edit, name='edit'),
        url(r'^save/(?P<id>\d+)/$', views.ability_save, name='save'),
        url(r'^ask_del/(?P<id>\d+)/$', views.ability_ask_del, name='ask_del'),
        url(r'^delete/(?P<id>\d+)/$', views.ability_delete, name='delete'),
    )
