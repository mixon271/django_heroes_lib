# -*- coding: utf-8 -*-

from django.db import models

class Ability(models.Model):
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=255, default='No Description.')
    # TODO: Добавить поле с изображением

    def __unicode__(self):
        return self.name
