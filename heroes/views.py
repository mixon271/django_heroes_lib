from django.shortcuts import render
from heroes.models import Ability
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template import Context, loader
from django.core.urlresolvers import reverse

def ability_index(request):
    abilities = Ability.objects.all()
    context = {'abilities': abilities}
    return render(request, 'abilities/index.html', context)

def ability_detail(request, id):
    ability = Ability.objects.filter(pk=id).first()

    if not ability:
        raise Http404

    context = {'ability': ability}
    return render(request, 'abilities/detail.html', context)

def ability_new(request):
    ability = Ability.objects.create()
    context = {
        'is_new': True,
        'ability': ability,
        'title': 'Creating New Ability',
    }

    return render(request, 'abilities/edit.html', context)

def ability_edit(request, id):
    ability = Ability.objects.filter(pk=id).first()
    title = 'Edit of "%s"' % ability.name

    if not ability:
        raise Http404

    context = {
        'is_new': False,
        'ability': ability,
        'title': title
        }
    return render(request, 'abilities/edit.html', context)

def ability_save(request, id):
    ability = Ability(
        id=id,
        name=request.POST['name'],
        description=request.POST['description']
    )
    ability.save()
    return HttpResponseRedirect(reverse('abilities:index'))

def ability_ask_del(request, id):
    ability = Ability.objects.filter(pk=id).first()

    if not ability:
        raise Http404

    context = {
        'ability': ability
    }

    return render(request, 'abilities/delete.html', context)

def ability_delete(request, id):
    Ability.objects.filter(pk=id).delete()
    return HttpResponseRedirect(reverse('abilities:index'))
