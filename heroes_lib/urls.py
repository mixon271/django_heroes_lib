from django.conf.urls import patterns, include, url
from django.contrib import admin
import heroes.urls

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'heroes_lib.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^abilities/', include(heroes.urls, namespace='abilities'))
)
